
var tasks = document.getElementsByClassName('task');

for (var i = 0; i < tasks.length; i++) {
    tasks[i].onclick = function() {
        document.location = "detail.html";
    }
}

var complete_buttons = document.getElementsByClassName('task-complete');
for (var i = 0; i < complete_buttons.length; i++) {
    complete_buttons[i].onclick = function(e) {
        e.stopPropagation();
    }
}

var delete_buttons = document.getElementsByClassName('fa-trash-alt');
for (var i = 0; i < delete_buttons.length; i++) {
    delete_buttons[i].onclick = function(e) {
        e.stopPropagation();
    }
}